var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var mongoose   = require('mongoose');
var Book       = require('./Book.model');

var port = 8080;

/* =====================================
** Connect to MongoDB
** =====================================
*/
var db = 'mongodb://localhost/example';
mongoose.connect(db);

/* =====================================
** Use BodyParser
** =====================================
*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/* =====================================
** Define Your Router
** =====================================
*/
app.get('/', function (req, res, next) {
    res.send('Happy to be here');
});

app.get('/books', function (req, res, next) {
    console.log('Getting all Books');
    Book.find({})
        .exec(function (err, books) {
            if (err) throw err;

            console.log(books);

            res.json(books);
        });
});

app.get('/books/:id', function (req, res, next) {
    console.log('Getting a Book');
    Book.findOne({
        _id: req.params.id
    }).exec(function (err, book) {
        if (err) throw err;

        console.log(book);

        res.json(book);
    });
});

app.post('/book', function (req, res, next) {
    var newBook = new Book();

    newBook.title    = req.body.title;
    newBook.author   = req.body.author;
    newBook.category = req.body.category;

    newBook.save(function (err, book) {
        if (err) throw err;

        console.log(book);
        res.send(JSON.stringify(book));
    })
});

app.post('/book2', function (req, res, next) {
    Book.create(req.body, function (err, book) {
        if (err) throw err;

        console.log(book);
        res.send(JSON.stringify(book)); 
    })
});

app.put('/book/:id', function (req, res, next) {
    Book.findOneAndUpdate({
        _id: req.params.id
    },
    {
        $set: {
            title: req.body.title
        }
    },
    {
        upsert: true
    },
    function (err, newBook) {
        if (err) throw err;
        res.send(JSON.stringify(newBook));
    });
});

app.delete('/book/:id', function (req, res, next) {
    Book.findOneAndRemove({
        _id: req.params.id
    },
    function (err) {
        if (err) throw err;

        res.send('Deleted Book Successfully');
    });
});

app.listen(port, function() {
    console.log('App is running on port: ', port);
});
