var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var path = require('path');
var userController = require('./controllers/user.controller');

var port = 8080;

/* ========================
** Connect db with mongoose
*/
var db = 'mongodb://localhost/userExample';
mongoose.connect(db);

app.use(express.static(path.join(__dirname, 'views')));

/* =======================
** Use BodyParser
*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

/* =======================
** Define Your Router
*/
app.get('/', function (req, res, next) {
    res.render('index.html');
});

app.post('/', userController.register);

app.listen(port, function () {
    console.log('Server is running on port: ' + port);
});