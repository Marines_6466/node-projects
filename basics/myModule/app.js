'use strict';

const Enigma = require('./enigma');

const eng = new Enigma('magrathea');

let encodeString = eng.encode("Don't Panic!");
let decodeString = eng.decode(encodeString);

console.log("Encoded: ", encodeString);
console.log("Decoded: ", decodeString);

let qr = eng.qrgen("http://www.npmjs.com", "outImage.png");

// Check qr code then log message
qr ? console.log('QR Code Generated!') : console.log('QR Code Failed!');