'use strict';

const http = require('http');
const url  = require('url');
const fs   = require('fs');
const path = require('path');

let mimes = {
    '.htm': 'text/html',
    '.css': 'text/css',
    '.js' : 'text/javascript',
    '.gif': 'image/gif',
    '.jpg': 'image/jpeg',
    '.png': 'image/png'
}

function webserver (req, res) {
    // If the route requested is '/', then load 'index.htm' or else
    // load the request file(s)
    let baseURI  = url.parse(req.url, true);
    let filePath = __dirname + (baseURI.pathname === '/' ? '/index.htm' : baseURI.pathname);

    // Check if the requested file is accessible or not
    fs.access(filePath, fs.F_OK, error => {
        if (!error) {
            // Read and serve the file over response
            fs.readFile(filePath, (error, content) => {
                if (!error) {
                    console.log('Serving: ', filePath);

                    // Resolve the content type
                    let contentType = mimes[path.extname(filePath)]; //mimes['.css'] === 'text/css'
                    // Serve the file from the buffer
                    res.writeHead(200, {'Content-type': contentType});
                    res.end(content, 'utf-8');
                } else {
                    // Serve a 500
                    res.writeHead(500);
                    res.end('The server could not read the file requested');
                }
            });
        } else {
            // Serve a 404
            res.writeHead(404);
            res.end('Content not found');
        }
    })

    // console.log(filePath);
}

http.createServer(webserver).listen(3000, () => {
    console.log('WebServer running on port 3000');
})